﻿using System;
using System.IO;
using System.Linq;
using GameLib;
using GameLib.GameObjects;
using GameLib.GameObjects.Components;
using GameLib.GameObjects.Components.Default;
using GameMaker;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GameMakerTests
{
    [TestClass]
    public class BaseObjectManagerTests
    {
        [TestMethod]
        public void AddMethodTest()
        {
            var gameObjectsManager = new BaseGameObjectsManager();
 
            gameObjectsManager.Add(new GameObject("GameObject"));

            Assert.IsTrue(gameObjectsManager.GetByName("GameObject") != null, "Object was not added or found");
        }

        [TestMethod]
        public void RemoveMethodTest()
        {
            var gameObjectsManager = new BaseGameObjectsManager();
            var gameObject = new GameObject("GameObject");
            gameObjectsManager.Add(gameObject);
            gameObjectsManager.Remove(gameObject);

            Assert.IsTrue(gameObjectsManager.GetByName("GameObject") == null, "Object was not removed by reference");

            gameObjectsManager.Add(gameObject);
            gameObjectsManager.Remove(gameObject.Name);

            Assert.IsTrue(gameObjectsManager.GetByName("GameObject") == null, "Object was not removed by name");
        }

        [TestMethod]
        public void CreateMethodTest()
        {
            var componentsManager = new ComponentsManager();
            var gameObjectsManager = new PrototypeManager();
            IGameObject gameObject = new GameObject("GameObject")
                                 {
                                     Transform = {X = 5, Y = 6}
                                 };
            //todo: add more components

            gameObjectsManager.Add(gameObject);

            gameObject = gameObjectsManager.CreateGameObject("GameObject");
            Assert.IsNotNull(gameObject, "Object was not created");
            Assert.IsTrue(gameObject.Transform.X == 5 && gameObject.Transform.Y == 6, "Object was not created correctly");

        }

        [TestMethod]
        public void RenameMethodTest()
        {
            var gameObjectsManager = new BaseGameObjectsManager();
            var gameObject = new GameObject("GameObject");
            gameObjectsManager.Add(gameObject);
            gameObjectsManager.Rename("GameObject", "G");

            Assert.IsTrue(gameObjectsManager.GetByName("G") != null, "Object was not renamed correctly");
        }

        [TestMethod]
        public void SaveLoadMethodTest()
        {
            var gameObjectsManager = new BaseGameObjectsManager();
            var componentsManager = new ComponentsManager();
            IGameObject gameObject = new GameObject("GameObject");

            gameObject.Transform.X = 10;

            gameObjectsManager.Add(gameObject);

            string directory = Directory.GetCurrentDirectory() + "//" + "gameobject.go";

            gameObjectsManager.Save(directory);

            gameObjectsManager = new BaseGameObjectsManager();
            gameObjectsManager.Load(directory, componentsManager);


            gameObject = gameObjectsManager.GetByName(gameObject.Name);
            Assert.IsNotNull(gameObject, "Object was not loaded");

            Assert.IsTrue(gameObject.Transform.X == 10, "Object data was not loaded correctly");
        }

        [TestMethod]
        public void AddAndCloneObjectTest()
        {
            IGameObject gameObject = new GameObject("someObject");
            gameObject.Transform.X = 15;
            gameObject.AddComponents(new PhysicsComponent());



            var componentsManager = new ComponentsManager();
            var prototypeManager = new PrototypeManager();

            prototypeManager.Add(gameObject);



            Assert.IsTrue(prototypeManager.GetGameObjects().Count != 0, "Game object data was not added");

            foreach (var item in prototypeManager.CloneObjects())
            {
                Assert.IsTrue(item.GetComponents<IComponent>().Count != 0, "Components data is empty");

                var trasform = item.Transform;
                Assert.IsTrue(trasform.X == 15, "value was not copied");

                foreach (var data in item.GetComponents<IComponent>())
                {
                    Assert.IsTrue(
                        gameObject.GetComponents<IComponent>().Any(p => data.Name == p.GetType().Name),
                        "Components were not found");

                   
                }
            }
        }

    }
}
