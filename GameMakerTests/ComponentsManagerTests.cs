﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using GameLib;
using GameLib.GameObjects;
using GameLib.GameObjects.Components;
using GameLib.GameObjects.Components.Default;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenTK;

namespace GameMakerTests
{
    [TestClass]
    public class ComponentsManagerTests
    {
        [TestMethod]
        public void ComponentAddTest()
        {
            var componentsManager = new ComponentsManager();

            componentsManager.AddType(typeof(PhysicsComponent));
            IComponent component = componentsManager.CreateComponent("PhysicsComponent");

            Assert.IsTrue(component != null, "Component was not created");
            Assert.IsTrue(component is PhysicsComponent, "Created component was not of excpected type");
        }
    }
}
