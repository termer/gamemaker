﻿using System;
using GameLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GameMakerTests
{
    [TestClass]
    public class GameLevelManagerTests
    {
        [TestMethod]
        public void SaveGameLevelTest()
        {
            var componentsManager = new ComponentsManager();
            var gameLevel = new GameLevel(componentsManager, "Level1", 640, 480, 32);

            //todo......
        }
        [TestMethod]
        public void AddLevelTest()
        {
            var componentsManager = new ComponentsManager();
            var gameLevelManager = new GameLevelManager(componentsManager);

            var level1 = gameLevelManager.AddLevel();
            var level2 = gameLevelManager.AddLevel();

            Assert.IsTrue(gameLevelManager.LoadedLevelsCount == 2, "Levels were not added");
            Assert.IsTrue(level1.Name == "Level1", "Level 1 name is wrong");
            Assert.IsTrue(level2.Name == "Level2", "Level 2 name is wrong");
        }
    }
}
