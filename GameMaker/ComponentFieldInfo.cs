﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameLib.GameObjects.Components;
using GameLib.GameObjects;
using System.Reflection;

namespace GameMaker
{
    public class ComponentFieldInfo
    {
        public IComponent Component;
        public FieldInfo Field;

        public ComponentFieldInfo(IComponent component, FieldInfo field)
        {
            Component = component;
            Field = field;
        }
    }
}
