﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Blueberry.Graphics;
using GameLib;
using GameLib.GameObjects;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace GameMaker
{

    // user events
    partial class MainForm
    {
        protected GameRunner _gameRunner;


        protected PrototypeManager _prototypeManager;
        protected GameLevelManager _gameLevelManager;
        protected ComponentsManagerCompilier _componentsManager;
        protected Project _project;

        protected void InitGameManagers()
        {
            _componentsManager = new ComponentsManagerCompilier();
            _componentsManager.AssemblyChanged += ComponentsManagerOnAssemblyChanged;

            _prototypeManager = new PrototypeManager();
            _gameLevelManager = new GameLevelManager(_componentsManager);

            _gameRunner = new GameRunner();
        }

        protected void glControl1_Paint(object sender, PaintEventArgs e)
        {
            if (!_loaded)
                return;
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            SpriteBatch.Instance.Begin();
            GameWorld.Draw(0);
            GameWorld.DrawInvicibleObjects();
            SpriteBatch.Instance.End();
            
            _glControl1.SwapBuffers();
        }

        protected void ComponentsManagerOnAssemblyChanged(object sender, Type[] newtypes, Type[] oldtypes)
        {
            _componentsView.Invoke((MethodInvoker)(() =>
                                                       {
                                                           _componentsView.Nodes.Clear();
                                                           foreach (var newtype in newtypes)
                                                           {
                                                               _componentsView.Nodes.Add(newtype.Name, newtype.Name);
                                                           }

                                                           _prototypeManager.RecreateComponents(_componentsManager, newtypes, oldtypes);
                                                           GameWorld.GameObjects.RecreateComponents(_componentsManager, newtypes, oldtypes);
                                                       }));

        }

        private void LevelTabControlOnTabChanged(object sender, TabChangedEventArguments e)
        {
            _gameLevelManager.CopyLevel(e.NewTab.TabText);
        }

        #region Main menu events
        protected void newProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_project != null)
            {
                //todo: save project
            }
            _project = new Project(_gameLevelManager, _prototypeManager, _componentsManager)
            {
                Name = "TestProject",
                Directory = Directory.GetCurrentDirectory(),
                ComponentsDirectory = "Components",
                ObjectsDirectory = "GameObjects",
                LevelsDirectory = "Levels"
            };
            // _project.ResoursesDirectory = "Resourses";
            _project.Save();//todo add dialog for changing saving directory and name
        }

        protected void runToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _gameRunner.Run();
        }

        protected void stopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _gameRunner.Stop();
        }
        #endregion

        #region TreeViews node double click events
        protected void GameObjectsViewOnNodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            var obj = _prototypeManager.GetByName(e.Node.Text);
            Debug.Assert(obj != null, "Object with name " + e.Node.Text + " was not found");

            SetObjectForPropertiesDock(obj);
            _objectPropertiesDock.Show();
        }




        protected void ComponentsViewOnNodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            throw new NotImplementedException();
        }

        protected void LevelsViewOnNodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            throw new NotImplementedException();
        } 
        #endregion

        #region Views edit label event
        protected void GameObjectsViewOnAfterLabelEdit(object sender, NodeLabelEditEventArgs e)
        {
            if(e.Label == null)
                return;
            _prototypeManager.Rename(e.Node.Text, e.Label);
        }
        #endregion

        #region Add click event
        protected void ComponentsAddClickEvent(object sender, EventArgs eventArgs)
        {
            throw new NotImplementedException();
            _componentsView.Nodes.Add("Component1", "Component");
            _componentsView.Nodes[0].BeginEdit();
        }
        
        protected void GameObjectsAddClickEvent(object sender, EventArgs eventArgs)
        {
            _gameObjectsView.Nodes.Add("GameObject", "GameObject");
            _gameObjectsView.Nodes[_gameObjectsView.Nodes.Count - 1].BeginEdit();

            _prototypeManager.Add(new GameObject("GameObject"));
        }

        protected void LevelsAddClickEvent(object sender, EventArgs eventArgs)
        {
            var gameLevel = _gameLevelManager.AddLevel();
            _levelTabControl.AddLevelTab(gameLevel.Name);
        }

        #endregion

        #region remove click event
        protected void ComponentsRemoveClickEvent(object sender, EventArgs eventArgs)
        {
            throw new NotImplementedException();
        }



        protected void GameObjectsRemoveClickEvent(object sender, EventArgs eventArgs)
        {
            throw new NotImplementedException();
        }

        protected void LevelsRemoveClickEvent(object sender, EventArgs eventArgs)
        {
            throw new NotImplementedException();
        }


        #endregion





    }
}
