﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Blueberry.Graphics;
using GameLib;
using GameLib.GameObjects;
using GameLib.GameObjects.Components.Default;
using GameLib.GameObjects.Graphics;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using WeifenLuo.WinFormsUI.Docking;

namespace GameMaker
{
    public partial class MainForm : Form
    {
        private LevelTabControl _levelTabControl;

        private bool _loaded = false;
        private DockPanel _dockPanel;
        private TreeView _componentsView;
        private TreeView _gameObjectsView;
        private TreeView _levelsView;

        private DockContent _objectPropertiesDock;

        private GLControl _glControl1;


        public MainForm()
        {
            CreateGlControl();

            InitializeComponent();

            CreateTrees();
            CreateDockingPanels();

            InitGameManagers();

            _levelTabControl = new LevelTabControl(_dockPanel, _glControl1);
            _levelTabControl.TabChanged += LevelTabControlOnTabChanged;
        }



        private void CreateGlControl()
        {
            _glControl1 = new GLControl
                              {
                                  BackColor = Color.Black,
                                  Dock = DockStyle.Fill,
                                  Location = new Point(0, 24),
                                  Name = "glControl1",
                                  Size = new Size(1189, 667),
                                  TabIndex = 4,
                                  VSync = false,
                                  AllowDrop = true
                              };
            _glControl1.Load += glControl1_Load;
            _glControl1.Paint += glControl1_Paint;
            _glControl1.DragDrop += GlControl1OnDragDrop;
            _glControl1.DragOver += GlControl1OnDragOver;
        }




        private void CreateDockingPanels()
        {
            //main panel
            _dockPanel = new DockPanel {Dock = DockStyle.Fill};
            Controls.Add(_dockPanel);
            _dockPanel.BringToFront();


            //CreateDockContent(_dockPanel, "Level editor", DockState.Document, _glControl1);


            CreateDockContent(_dockPanel, "Components", DockState.DockLeftAutoHide, _componentsView,
                              CreateControlMenuStrip(ComponentsAddClickEvent,ComponentsRemoveClickEvent));


            CreateDockContent(_dockPanel, "Game objects", DockState.DockLeftAutoHide, _gameObjectsView,
                              CreateControlMenuStrip(GameObjectsAddClickEvent,GameObjectsRemoveClickEvent));

            CreateDockContent(_dockPanel, "Levels", DockState.DockRightAutoHide, _levelsView,
                              CreateControlMenuStrip(LevelsAddClickEvent, LevelsRemoveClickEvent));
            _objectPropertiesDock = CreateDockContent(_dockPanel, "Object properties", DockState.DockRightAutoHide);
        }




        private void CreateTrees()
        {
            _componentsView = CreateFillingTree("Components");
            _componentsView.NodeMouseDoubleClick += ComponentsViewOnNodeMouseDoubleClick;
            
            _gameObjectsView = CreateFillingTree("Game objects");
            _gameObjectsView.NodeMouseDoubleClick += GameObjectsViewOnNodeMouseDoubleClick;
            _gameObjectsView.AfterLabelEdit += GameObjectsViewOnAfterLabelEdit;
            _gameObjectsView.MouseDown += GameObjectsViewOnMouseDown;


            _levelsView = CreateFillingTree("Levels");

            _levelsView.NodeMouseDoubleClick += LevelsViewOnNodeMouseDoubleClick;
        }


        private void GlControl1OnDragDrop(object sender, DragEventArgs e)
        {
            string objectName = (string)e.Data.GetData(typeof(string));

            IGameObject gameObject = _prototypeManager.CreateGameObject(objectName);

            GameLevel gameLevel = _gameLevelManager.GetLoadedLevel(_levelTabControl.ActiveTabName);
            gameLevel.AddGameObject(gameObject);

            GameWorld.GameObjects.Add(gameObject);
            Refresh();
        }

        private void GlControl1OnDragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void GameObjectsViewOnMouseDown(object sender, MouseEventArgs e)
        {
            _glControl1.DoDragDrop(_gameObjectsView.SelectedNode.Text, DragDropEffects.Move);
        }


        private TreeView CreateFillingTree(string name)
        {
            return new TreeView
                       {
                           Name = name,
                           AutoSize = true,
                           TabIndex = 0,
                           Dock = DockStyle.Fill,
                           LabelEdit = true
                       };
        }

        private DockContent CreateDockContent(DockPanel dockPanel, string text, DockState dockState, params Control[] controls)
        {
            var dockContent = new DockContent {ShowHint = dockState, TabText = text, HideOnClose = true};
            dockContent.Controls.AddRange(controls);
            dockContent.Show(dockPanel);
            return dockContent;
        }

        /// <summary>
        /// creates menustrip with buttons add and remove
        /// </summary>
        private MenuStrip CreateControlMenuStrip(EventHandler addClickEvent, EventHandler removeClickEvent)
        {
            var menuStrip = new MenuStrip
                                {
                                    Dock = DockStyle.Top
                                };


            var addButton = new ToolStripMenuItem
                                {
                                    Text = "Add", Size = new Size(30, 20)
                                };
            addButton.Click += addClickEvent;
            menuStrip.Items.Add(addButton);

            var removeButton = new ToolStripMenuItem
                                   {
                                       Text = "Remove", Size = new Size(30, 20)
                                   };
            removeButton.Click += removeClickEvent;
            menuStrip.Items.Add(removeButton);

            return menuStrip;
        }



        private void glControl1_Load(object sender, EventArgs e)
        {
            _loaded = true;

            GL.ClearColor(Color4.CornflowerBlue);
            GL.Viewport(0, 0, _glControl1.Width, _glControl1.Height);
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);


        }




        private void SetObjectForPropertiesDock(IGameObject gameObject)
        {
            _objectPropertiesDock.Controls.Clear();
            int offset = 16;
            const int xoffset = 6;

            foreach (var component in gameObject.GetComponents<GameLib.GameObjects.Components.IComponent>())
            {
                var groupBox = new GroupBox
                                   {
                                       Location = new Point(0, offset),
                                       Text = component.Name,
                                       AutoSize = true
                                   };


                foreach (var field in component.Fields)
                {
                    var label = new Label
                                    {
                                        Text = field.Name,
                                        Location = new Point(xoffset, offset)
                                    };
                    var textBox = new NumericUpDown
                                      {
                                          Value = (decimal)(float)component[field],
                                          Location = new Point(label.Width + xoffset, offset)
                                      };

                    offset += Math.Max(textBox.Height, label.Height);
                    textBox.Tag = new ComponentFieldInfo(component, field);
                    textBox.TextChanged += textBox_TextChanged;

                    groupBox.Controls.Add(label);
                    groupBox.Controls.Add(textBox);
                }

                _objectPropertiesDock.Controls.Add(groupBox);
                
            }
        }

        void textBox_TextChanged(object sender, EventArgs e)
        {
            var textBox = (NumericUpDown)sender;

            var componentFieldInfo = (ComponentFieldInfo)textBox.Tag;

            componentFieldInfo.Component[componentFieldInfo.Field] = (Single)textBox.Value;

            
        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }
    }
}
