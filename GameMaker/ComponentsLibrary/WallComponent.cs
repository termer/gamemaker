﻿using GameLib.GameObjects.Components.Default;

namespace GameMaker.ComponentsLibrary
{
    public class WallComponent:PhysicsComponent
    {
        public WallComponent(World physicsWorld)
            : base(physicsWorld)
        {
            Body.BodyType = BodyType.Static;
        }

        public override void ProccesMessage(StandartComponents.Messages.IMessage message)
        {
        }
    }
}
