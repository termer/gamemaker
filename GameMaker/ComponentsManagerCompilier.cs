﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using GameLib;
using GameLib.GameObjects;
using GameLib.GameObjects.Components;
using Microsoft.CSharp;

namespace GameMaker
{
    public class ComponentsManagerCompilier:BaseComponentsManager, IDisposable
    {
        private string _directory;
        public override string Directory
        {
            get 
            {
                return _directory; 
            }
            set 
            {
                _directory = value;

                if (_watcher != null)
                    _watcher.EnableRaisingEvents = false;
                else
                {
                    _watcher = new FileSystemWatcher(_directory, "*.cs");

                    _watcher.Changed += WatcherOnChanged;
                    _watcher.Deleted += WatcherOnDeleted;
                    _watcher.Created += WatcherOnCreated;
                    _watcher.EnableRaisingEvents = true;
                }

                _watcher.Path = _directory;
                Compile();
            }
        }

        private FileSystemWatcher _watcher;

        public void Compile()
        {
            bool succes = false;
            while (!succes)
            {
                try
                {
                    var paths = System.IO.Directory.GetFiles(_directory, "*.cs");
                    var codeText = new string[paths.Length];


                    for (int i = 0; i < paths.Length; i++)
                    {
                        codeText[i] = File.ReadAllText(paths[i]);
                    }

                    var providerOptions = new Dictionary<string, string>
                                              {
                                                  {"CompilerVersion", "v4.0"}
                                              };
                    var codeProvider = new CSharpCodeProvider(providerOptions);

                    var options = new CompilerParameters
                                      {
                                          GenerateInMemory = true,
                                      };
                    options.ReferencedAssemblies.Add("GameLib.dll");
                    options.ReferencedAssemblies.Add("BlueberryEngine.dll");
                    options.ReferencedAssemblies.Add("System.dll");
                    options.ReferencedAssemblies.Add("System.Drawing.dll");
                    options.ReferencedAssemblies.Add("System.Data.dll");
                    options.ReferencedAssemblies.Add("System.Core.dll");
                    options.ReferencedAssemblies.Add("FarseerPhysics.dll");
                    options.ReferencedAssemblies.Add("OpenTK.dll");
                    //todo: add needed dlls

                    var results = codeProvider.CompileAssemblyFromSource(options, codeText);

                    if (results.Errors.Count != 0)
                        throw new CompilationException("Compilation failed due to code errors", results.Errors);

                    var types = results.CompiledAssembly.GetExportedTypes();
                    var oldTypes = Components.Values.ToArray();


                    Components.Clear();
                    foreach (var type in types)
                        AddType(type);
                    OnAssemblyChanged(this, Components.Values.ToArray(), oldTypes);
                    succes = true;
                }
                catch (IOException e)
                {
                    succes = false;
                }
            }
        }



        private void WatcherOnCreated(object sender, FileSystemEventArgs e)
        {
            Compile();
        }
        private void WatcherOnDeleted(object sender, FileSystemEventArgs e)
        {
            Compile();
        }
        private void WatcherOnChanged(object sender, FileSystemEventArgs e)
        {
            Compile();
        }


        public class CompilationException : Exception
        {
            public CompilerErrorCollection Errors { get; private set; }

            public CompilationException(string message, CompilerErrorCollection errors)
                : base(message)
            {
                Errors = errors;
            }
        }

        public void Dispose()
        {
            _watcher.EnableRaisingEvents = false;
        }
    }
}
