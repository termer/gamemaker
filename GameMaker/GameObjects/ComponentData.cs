using System;
using System.Reflection;
using GameLib.GameObjects.Components;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml;
using System.Linq;

namespace GameLib.GameObjects
{
    public class ComponentData
    {
        public string ComponentName { get; private set; }
        public FieldInfo[] ComponentFields;
        
        private readonly Dictionary<string, object> _variables;
        private IComponent _component;
        
        
        public object this[string name]
        {
            get
            {
                return _variables[name];
            }
            set
            {
                _variables[name] = value;
            }
        }
        public object this[FieldInfo field]
        {
            get 
            {
                return _variables[field.Name] ;
            }
            set
            {
                SetValue(field.Name, value);
            }
        }

        
        public ComponentData()
        {
            _variables = new Dictionary<string, object>();
        }
        
        public void SetComponent(IComponent component)
        {
            _component = component;
            
            var type = _component.GetType();
            ComponentFields = type.GetFields();
            ComponentName = type.Name;
            
            UpdateComponentVars();
        }
        
        public void SetValue(string name, object value)
        {
            _variables[name] = value;
            SetComponentVariable(name, value);
        }
        
        
        private void UpdateComponentVars()
        {
            //Debug.Assert(ComponentFields.Length < _variables.Count, "Too many vars");
            
            foreach (var field in ComponentFields)
            {
                try
                {
                    field.SetValue(_component, _variables[field.Name]);
                }
                catch (Exception)
                {
                    _variables.Add(field.Name, field.GetValue(_component));   
                }
            }
        }
        
        private void SetComponentVariable(string name, object value)
        {
            ComponentFields.First(c => c.Name == name).SetValue(_component, value);
        }
        
        
        public void Save(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("Component");
            xmlWriter.WriteAttributeString("Name", ComponentName);
            foreach (var item in _variables.Keys)
            {
                xmlWriter.WriteAttributeString(item, _variables[item].ToString());
            }
            xmlWriter.WriteEndElement();
        }
        
        public void Load(XmlNode node)
        {
            foreach (XmlAttribute attribute in node)
            {
                Type type = Type.GetType(attribute.Value);
                _variables[attribute.Name] = Activator.CreateInstance(type);
            }
            UpdateComponentVars();
        }
        
        internal void Draw()
        {
            if (_component is IDrawable)
                (_component as IDrawable).Draw(0f);
        }
    }
}

