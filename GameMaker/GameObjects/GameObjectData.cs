using System.Collections.Generic;
using System.Xml;
using GameLib.GameObjects.Components;

namespace GameLib.GameObjects
{
    public class GameObjectData
    {
        public readonly List<ComponentData> ComponentsData;
        public IGameObject GameObject { get; private set; }
        
        protected IComponentsManager ComponentsManager;
        
        public GameObjectData(IComponentsManager componentsManager, IGameObject gameObject)
        {
            ComponentsData = new List<ComponentData>();
            ComponentsManager = componentsManager;
            SetGameObject(gameObject);
        }
        


        public void RecreateComponents()
        {
            GameObject.ClearComponents();
            
            foreach (var component in ComponentsData)
            {
                component.SetComponent(ComponentsManager.CreateComponent(component.ComponentName));
                //todo: prbably add components back to object?
            }
        }

        public void AddComponent(string name)
        {
            var componentsData = new ComponentData();
            componentsData.SetComponent(ComponentsManager.CreateComponent(name));
            ComponentsData.Add(componentsData);
        }

        public void Save(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("GameObject");
            xmlWriter.WriteAttributeString("Name", GameObject.Name);
            xmlWriter.WriteStartElement("Components");
            foreach (var item in ComponentsData)
            {
                item.Save(xmlWriter);
            }
            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndElement();
        }
        
        public void Load(XmlNode node)
        {
            var gameObject = new GameObject(node.Attributes["Name"].Value);
            var componentsNode = node.SelectSingleNode("Components");
            var componentsNodes = componentsNode.SelectNodes("Component");
            foreach (XmlNode item in componentsNode)
            {
                var componentData = new ComponentData();
                var component = ComponentsManager.CreateComponent(node.Attributes["Name"].Value);
                componentData.SetComponent(component);
                componentData.Load(item);
            }
        }
        
        internal void Draw()
        {
            foreach (var component in ComponentsData)
            {
                component.Draw();
            }
        }

        private void SetGameObject(IGameObject gameObject)
        {
            GameObject = gameObject;
            ComponentsData.Clear();

            foreach (var component in gameObject.GetComponents<IComponent>())
            {
                var componentData = new ComponentData();
                componentData.SetComponent(component);
                ComponentsData.Add(componentData);
            }
        }
    }
}