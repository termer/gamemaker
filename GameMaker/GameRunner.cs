﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using GameLib;

namespace GameMaker
{
    public class GameRunner
    {
        public bool Running { get; private set; }

        private Thread _gameThread;
        private Stopwatch _dtWatch;

        public GameRunner()
        {
            _gameThread = new Thread(GameThreadMethod);

            _dtWatch = new Stopwatch();
        }

        public void Run()
        {
            Running = true;
            _gameThread.Start();
        }

        public void Stop()
        {
            Running = false;
        }

        private void GameThreadMethod()
        {
            while(Running)
            {
                float dt = ComputeTimeSlice();
                GameWorld.Update(dt);
            }
            _dtWatch.Stop();
            _dtWatch.Reset();
        }

        private float ComputeTimeSlice()
        {
            _dtWatch.Stop();
            var timeslice = (float)_dtWatch.Elapsed.TotalMilliseconds;
            _dtWatch.Reset();
            _dtWatch.Start();
            return timeslice;
        }



    }
}
