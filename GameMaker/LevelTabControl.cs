﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK;
using WeifenLuo.WinFormsUI.Docking;

namespace GameMaker
{
    public delegate void TabChangedEventHandler(object sender, TabChangedEventArguments eventArguments);

    public class TabChangedEventArguments : EventArgs
    {
        public DockContent PreviousTab { get; private set; }
        public DockContent NewTab { get; private set; }

        public TabChangedEventArguments(DockContent previousTab, DockContent newTab)
        {
            PreviousTab = previousTab;
            NewTab = newTab;
        }
    }



    public class LevelTabControl
    {
        public string ActiveTabName { get { return _activeTab.TabText; } }

        public event TabChangedEventHandler TabChanged;

        private readonly List<DockContent> _tabs;
        private readonly DockPanel _dockPanel;
        private readonly GLControl _glControl;

        private DockContent _activeTab;

        public LevelTabControl(DockPanel dockPanel, GLControl glControl)
        {
            _dockPanel = dockPanel;
            _glControl = glControl;
            _tabs = new List<DockContent>();
        }

        public void AddLevelTab(string name)
        {
            var tab = new DockContent
                                  {
                                      ShowHint = DockState.Document,
                                      DockAreas = DockAreas.Document,
                                      TabText = name,
                                      HideOnClose = false,
                                      CloseButtonVisible = true
                                  };
            tab.Controls.Add(_glControl);
            tab.Show(_dockPanel);

            tab.Activated += TabOnActivated;

            _tabs.Add(tab);

            _activeTab = tab;
        }

        private void TabOnActivated(object sender, EventArgs e)
        {
            var newTab = (sender as DockContent);
            newTab.Controls.Add(_glControl);

            TabChanged(this, new TabChangedEventArguments(_activeTab, newTab));

            _activeTab.Controls.Clear();
            _activeTab = newTab;


        }
    }
}
