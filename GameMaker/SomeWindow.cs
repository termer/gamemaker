using System;
using GameLib;
using GameLib.GameObjects.Components;
using GameLib.GameObjects.Components.Drawable;
using GameLib.GameObjects.Graphics;
using Blueberry.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Graphics;

namespace GameMaker
{
	public partial class SomeWindow : Gtk.Window
	{
		private GameWorld _gameWorld;

		public SomeWindow () : 
				base(Gtk.WindowType.Toplevel)
		{
			this.Build ();




		}

		private void InitGame ()
		{
			_gameWorld = new GameWorld(new OpenTK.Vector2(0, 9.8f));

			var gameObject = new GameObject(_gameWorld, "ololo");
			gameObject.AddComponents(new SpriteComponent(gameObject, new Sprite(new Texture("someGuy.png"))));
			_gameWorld.AddObject(gameObject);
		}


		protected void OnGlwidget1RenderFrame (object sender, EventArgs e)
		{
			_gameWorld.Draw(0f);
		}

		protected void OnGlwidget1GraphicsContextInitialized (object sender, EventArgs e)
		{
			GL.ClearColor(Color4.CornflowerBlue);
			GL.Viewport(0, 0, 320, 240);
			GL.Enable(EnableCap.Blend);
			GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

			InitGame();
		}
	}
}

