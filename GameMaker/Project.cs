﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using GameLib;
using GameLib.GameObjects;

namespace GameMaker
{
    public class Project
    {


        private readonly GameLevelManager _gameLevelManager;
        private readonly PrototypeManager _prototypeManager;
        private readonly IComponentsManager _componentsManager;


        public string Name;
        public string Directory;


        private string _componentsDirectory;
        public string ComponentsDirectory
        {
            get { return _componentsDirectory; }
            set
            {
                _componentsManager.Directory = CreateRelativePath(value);
                _componentsDirectory = value;
            }
        }

        public string _objectsDirectory;
        public string ObjectsDirectory
        {
            get { return _objectsDirectory; }
            set
            {
                //_staticObjectManager.Directory = CreateRelativePath(value);
                _objectsDirectory = value;
            }
        }

        private string _levelsDirectory;
        public string LevelsDirectory
        {
            get { return _gameLevelManager.Directory; }
            set
            {
                _gameLevelManager.Directory = CreateRelativePath(value);
                _levelsDirectory = value;
            }
        }

        public string ResoursesDirectory
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public Project(GameLevelManager gameLevelManager,
                       PrototypeManager prototypeManager,
                       IComponentsManager componentsesManager)
        {
            _gameLevelManager = gameLevelManager;
            _prototypeManager = prototypeManager;
            _componentsManager = componentsesManager;
        }

        
        public void Save()
        {
            using (XmlWriter xmlWriter = XmlWriter.Create(Directory + Name + ".prj"))
            {
                //xmlWriter.WriteWhitespace("\n");
                xmlWriter.WriteStartElement("Project");
                xmlWriter.WriteWhitespace("\n\t");
                xmlWriter.WriteElementString("Name", Name);
                xmlWriter.WriteWhitespace("\n\t");
                xmlWriter.WriteElementString("ComponentsDirectory", _componentsDirectory);
                xmlWriter.WriteWhitespace("\n\t");
                xmlWriter.WriteElementString("ObjectsDirectory", _objectsDirectory);
                xmlWriter.WriteWhitespace("\n\t");
                xmlWriter.WriteElementString("ResoursesDirectory", null);
                xmlWriter.WriteWhitespace("\n\t");
                xmlWriter.WriteElementString("LevelsDirectory", _levelsDirectory);
                xmlWriter.WriteWhitespace("\n");

                xmlWriter.WriteEndElement();
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="path">path to file</param>
        public void Load(string path)
        {
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(path);

            XmlNode node = xmlDocument.SelectSingleNode("Project");

            Name = node.SelectSingleNode("Name").Value;
            ComponentsDirectory = node.SelectSingleNode("ComponentsDirectory").Value;
            ResoursesDirectory = node.SelectSingleNode("ResoursesDirectory").Value;
            LevelsDirectory = node.SelectSingleNode("LevelsDirectory").Value;

            Directory = path;
        }

        private string CreateRelativePath(string value)
        {
            var fullDirectory = Directory + "\\" + value;
            if (!System.IO.Directory.Exists(fullDirectory))
                System.IO.Directory.CreateDirectory(fullDirectory);
            return fullDirectory;
        }
    }
}
