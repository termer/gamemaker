//using System;
//using GameLib;
//using Gtk;
//using GameLib.GameObjects.Components;
//using GameLib.GameObjects;
//using System.Collections.Generic;

//namespace GameMaker
//{
//    public partial class EditObjectWindow : Gtk.Window
//    {
//        protected class ComponentGuiGroup
//        {
//            public Entry NameLabel;
//            public List<Label> Labels;
//            public List<Entry> Entries;

//            private EditableComponentsData _component;

//            private readonly Fixed _fixed;

//            public ComponentGuiGroup(EditableComponentsData component, Fixed @fixed, ref int offset)
//            {
                
//                _fixed = @fixed;

//                Labels = new List<Label>();
//                Entries = new List<Entry>();


//                SetComponent(component, ref offset);
//            }

//            public void SetComponent(EditableComponentsData component, ref int offset)
//            {
//                _component = component;

//                AddComponentBeginning(ref offset);
//                foreach (var field in _component.ComponentFields)
//                {
//                    AddComponentField(field.Name, _component[field].ToString(), ref offset);
//                }
//            }

//            private void AddComponentBeginning(ref int offset)
//            {
//                NameLabel = new Entry(_component.GetType().Name);
//                offset += NameLabel.HeightRequest;
//                _fixed.Add(NameLabel);
//                var w1 = ((Fixed.FixedChild)(_fixed[NameLabel]));
//                w1.X = 15;
//                w1.Y = offset;
//                offset += 45;
//            }

//            private void AddComponentField(string name, string value, ref int offset)
//            {
//                var label = new Label(name);
//                var entry = new Entry(value);

//                _fixed.Add(label);
//               // label.Show();
//                var w1 = ((Fixed.FixedChild)(_fixed[label]));
//                w1.X = 15;
//                w1.Y = offset;
//                offset += 45;

//                _fixed.Add(entry);
//                w1 = ((Fixed.FixedChild)(_fixed[entry]));
//                w1.X = 15;
//                w1.Y = offset;
//                offset += 45;
//            }
//        }

//        private Button _addButton;

//        private readonly EditableGameObject _gameObjectData;
//        private readonly BaseComponentsManager _componentsManager;
//        private readonly List<ComponentGuiGroup> _componentGuiGroups;

//        private int _offset;

//        public EditObjectWindow(EditableGameObject gameObjectData, BaseComponentsManager componentsManager) : 
//                base(WindowType.Toplevel)
//        {
//            _gameObjectData = gameObjectData;
//            _componentsManager = componentsManager;
//            _componentGuiGroups = new List<ComponentGuiGroup>();

//            componentsManager.AssemblyChanged += ComponentsManagerOnAssemblyChanged;

            
//            this.Build();
//            UpdateComponents();
//        }

//        private void ComponentsManagerOnAssemblyChanged(object sender, Type[] newTypes, Type[] oldTypes)
//        {
//            throw new NotImplementedException();
//        }

//        protected void UpdateComponents()
//        {
//            Clear();

//            foreach (var item in _gameObjectData.ComponentsData)
//            {
//                var guiGroup = new ComponentGuiGroup(item, this.fixed6, ref _offset);
//                _componentGuiGroups.Add(guiGroup);
//            }

//            AddGuiEnding();
//            fixed6.ShowAll();
//            this.fixed6.QueueDraw();
//        }

//        private void AddGuiEnding()
//        {
//            _addButton = new Button();
//            _addButton.Pressed += AddButtonOnPressed;
//            _addButton.Label = "Add";
//            fixed6.Add(_addButton);
//            var w1 = ((Fixed.FixedChild)(fixed6[_addButton]));
//            w1.X = 15;
//            w1.Y = _offset;
//        }

//        private void AddButtonOnPressed(object sender, EventArgs eventArgs)
//        {

//        }


//        protected void AddComponent(string name)
//        {
//            _gameObjectData.AddComponent(name);
//        }

//        protected void RemoveComponent(string name)
//        {
//            throw new NotImplementedException();
//        }


//        protected void OnAddButtonActivated (object sender, EventArgs e)
//        {
//            throw new NotImplementedException ();
//        }

//        protected void OnRemoveButtonActivated (object sender, EventArgs e)
//        {
//            throw new NotImplementedException ();
//        }

//        protected void Clear()
//        {
//            _offset = 0;
//            _componentGuiGroups.Clear();
//            foreach (var child in fixed6)
//                fixed6.Remove((Widget)child); //will it work??
//        }

//        public override void Dispose()
//        {
//            base.Dispose();
//            _componentsManager.AssemblyChanged -= ComponentsManagerOnAssemblyChanged;
//        }

//    }
//}

