//using System;
//using System.IO;
//using Blueberry.Graphics;
//using OpenTK.Graphics.OpenGL;
//using OpenTK.Graphics;
//using GameLib;
//using GameLib.GameObjects;
//using Gtk;
//using System.Linq;
//using System.CodeDom.Compiler;
//namespace GameMaker
//{
//    public partial class AnotherWindow : Window
//    {
//        private GameWorld _gameWorld;
//        private readonly StaticObjectManager _staticObjectManager;
//        private readonly GameLevelManager _gameLevelManager;
//        private readonly ComponentsManagerCompilier _componentsManager;

//        private Project _project;

//        private ListStore _levelListStore;
//        private ListStore _componentsListStore;
//        private bool Run
//        {
//            get
//            {
//                return _run;
//            }
//            set
//            {
//                if(_run && !value)
//                    _didRun = true;
//                _run = value;
//                if(_run)
//                {
//                    GameThread();
//                }
//            }
//        }
//        private bool _run;
//        private bool _didRun;


//        public AnotherWindow () : 
//                base(Gtk.WindowType.Toplevel)
//        {

//            this.Build ();
          
//            _componentsManager = new ComponentsManagerCompilier();
//            _componentsManager.AssemblyChanged += ComponentsManagerOnAssemblyChanged;
//            _staticObjectManager = new StaticObjectManager(_componentsManager);
//            _gameLevelManager = new GameLevelManager(_componentsManager);
//            //_gameLevelManager.LoadLevelsInfo("Levels//");

//            CreateTreeViews();



//        }


//        private void InitGame ()
//        {
//            _gameWorld = new GameWorld(new OpenTK.Vector2(0, 9.8f));
			
//            //IGameObject gameObject = new GameObject(_gameWorld, "ololo");
//        //	gameObject.AddComponents(new SpriteComponent(new Sprite(new Texture("someGuy.png"))),
//        //	                         new PhysicsComponent(_gameWorld.PhysicsWorld));
//            //gameObject.Transform.X = 32;
//            //gameObject.Transform.Y = 32;
//         //	_gameWorld.AddGameObjectData(gameObject);
//            _gameWorld.Update(0f);
//        }

//        private void GameThread()
//        {
//            while (_run)
//            {
//                _gameWorld.Update(0.001f);
//                this.QueueDrawArea(glwidget1.Allocation.X, glwidget1.Allocation.Y,
//                                 glwidget1.Allocation.Width, glwidget1.Allocation.Height);
//                Application.RunIteration();
//            }
//        }

//        protected void OnGlwidget1Initialized (object sender, EventArgs e)
//        {

//            GL.ClearColor (Color4.CornflowerBlue);
//            GL.Viewport (0, 0, glwidget1.Allocation.Width, glwidget1.Allocation.Height);
//            GL.Enable (EnableCap.Blend);
//            GL.BlendFunc (BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
						
//            InitGame();
//        }

//        protected void CreateTreeViews ()
//        {
//            //LEVEL TREEVIEW......
//            var levelNameColumn = new TreeViewColumn {Title = "Level name"};
//            treeview3.AppendColumn (levelNameColumn);
//            _levelListStore = new ListStore (typeof (string));
//            treeview3.Model = _levelListStore;
//            var levelNameRendererCell = new CellRendererText ();
//            levelNameColumn.PackStart (levelNameRendererCell, true);
//            levelNameColumn.AddAttribute (levelNameRendererCell, "text", 0);


//            //COMPONENTS TREEVIEW.....
//            var componentNameColumn = new TreeViewColumn {Title = "Component name"};
//            ComponentsTreeView.AppendColumn(componentNameColumn);
//            _componentsListStore = new ListStore(typeof (string));
//            ComponentsTreeView.Model = _componentsListStore;
//            var componentsRendererCell = new CellRendererText();
//            componentNameColumn.PackStart(componentsRendererCell, true);
//            componentNameColumn.AddAttribute(componentsRendererCell, "text", 0);
//        }

//        protected void OnGlwidget1RenderFrame(object sender, EventArgs e)
//        {
//            GL.Clear(ClearBufferMask.ColorBufferBit);
//            //SpriteBatch.Instance.Begin();
//            //_gameWorld.Draw(0f);
//            //SpriteBatch.Instance.End();
//        }

//        protected void OnGlwidget1ShuttingDown(object sender, EventArgs e)
//        {
//        }

//        protected void OnYesAction1Activated(object sender, EventArgs e)
//        {
//        }



//        protected void OnYesAction2Activated(object sender, EventArgs e)
//        {
//            if(Run)
//                return;
//            Run = true;
//        }

//        protected void OnNotebook1SelectPage(object o, SelectPageArgs args)
//        {
//            //Run = false;

//        }

//        protected void OnNotebook1SwitchPage(object o, SwitchPageArgs args)
//        {
//            if (args.PageNum != 1)
//                Run = false;
//            else if (_didRun)
//                Run = true;
//        }

//        protected void OnNoAction1Activated(object sender, EventArgs e)
//        {
//            Run = false;
//            _didRun = false;
//        }

//        protected void OnAddActionActivated(object sender, EventArgs e)
//        {
//            if (_project == null)
//                return;

//            _gameLevelManager.AddLevel();
           
//            UpdateLevelTree();
//        }
//        protected void OnComponentAddButtonActivated(object sender, EventArgs e)
//        {
//            //todo: create new component
//        }


//        protected void NewProjectButtonPressed (object sender, EventArgs e)
//        {


//            if (_project != null)
//            {
//                //todo: save project
//            }
//            _project = new Project(_gameLevelManager, _staticObjectManager, _componentsManager)
//                           {
//                               Name = "TestProject",
//                               Directory = Directory.GetCurrentDirectory(),
//                               ComponentsDirectory = "Components",
//                               ObjectsDirectory = "GameObjects",
//                               LevelsDirectory = "Levels"
//                           };
//            // _project.ResoursesDirectory = "Resourses";
//            _project.Save();//todo add dialog for changing saving directory and name



//        }
        


//        protected void OnLoadProjectActionActivated (object sender, EventArgs e)
//        {
//            if (_project == null)
//                _project = new Project(_gameLevelManager, _staticObjectManager, _componentsManager);
//            //todo: add dialog for choosing project
//            _project.Load("TestProject");
//        }

//        protected void OnLevelsListRowActivated (object o, RowActivatedArgs args)
//        {
//            TreeIter iter;
//            _levelListStore.GetIter(out iter, new Gtk.TreePath(args.Path.Indices));

//            _gameLevelManager.LoadLevel ((string)_levelListStore.GetValue(iter, 0));
//        }

//        protected void OnComponentsListRowActivated(object o, RowActivatedArgs args)
//        {

//        }

//        protected void OnDelete(object o, DeleteEventArgs a)
//        {
//            Application.Quit();
//            a.RetVal = true;
//            Run = false;
//        }

//        protected void OnLoadComponentsMenuActivated (object sender, EventArgs e)
//        {
//            try
//            {
//                _componentsManager.Compile();
//            }
//            catch (ComponentsManagerCompilier.CompilationException ex)
//            {
//                foreach (CompilerError error in ex.Errors)
//                {
//                    Console.WriteLine(error.Line);
//                    Console.WriteLine(error.ErrorNumber);
//                    Console.WriteLine(error.ErrorText);
//                }
//            }
//        }

//        protected void UpdateLevelTree()
//        {
//            _levelListStore.Clear();
//            var levelsInfo = _gameLevelManager.GetLevelNames();

//            if (levelsInfo != null)
//                foreach (var item in levelsInfo)
//                    _levelListStore.AppendValues(item);
//        }

//        protected void UpdateComponentsTree(Type[] types)
//        {
//            _componentsListStore.Clear();
//            foreach (var type in types)
//            {
//                _componentsListStore.AppendValues(type.Name);
//            }
//        }

//        private void ComponentsManagerOnAssemblyChanged(object sender, Type[] newTypes, Type[] oldTypes)
//        {
//            UpdateComponentsTree(newTypes);
//        }



//        protected void OnAddObjectButtonActivated (object sender, EventArgs e)
//        {
//            var gameObjectData = new EditableGameObject(_componentsManager, new GameObject(_gameWorld, "AwesomeObject"));

//            var editObjectWindow = new EditObjectWindow(gameObjectData, _componentsManager);
//            editObjectWindow.Show();    

//        }

//    }


//}

