using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.IO;

namespace GameLib
{
    public class GameLevelManager
    {
        public string Directory;

        private readonly IComponentsManager _componentsManager;

        private GameLevelInfo[] _levelFiles;

        protected List<GameLevel> LoadedLevels;                                               

        public int LevelsCount
        {
            get 
			{
                return _levelFiles.Length;
            }
        }

        public int LoadedLevelsCount
        {
            get { return LoadedLevels.Count; }
        }

        public GameLevelManager(IComponentsManager componentsManager)
        {
            _componentsManager = componentsManager;
            LoadedLevels = new List<GameLevel>();
        }


        /// <summary>
        /// Creates new level, and adds it to memory
        /// </summary>
        /// <returns>created level</returns>
        public GameLevel AddLevel()
        {
            int k = 0;
            while (!LevelNameExists(Directory + "\\", "Level" + (++k), ".lvl")) ;

            var level = new GameLevel(_componentsManager, "Level" + k, 640, 480, 64);
            LoadedLevels.Add(level);

            return level;
        }

        public GameLevel GetLoadedLevel(string name)
        {
            return LoadedLevels.FirstOrDefault(level => level.Name == name);
        }

        public void CopyLevel(string name)
        {
            LoadedLevels.First(p => p.Name == name).CopyLevelToGameWorld();
        }

        public void LoadLevelsInfo(string directory)
        {
            var files = System.IO.Directory.GetFiles(directory, "*.lvl");

            _levelFiles = new GameLevelInfo[files.Length];
            for (int i = 0; i < files.Length; i++)
                _levelFiles[i].Path = files[i];
        }

        public void LoadLevel(string Name)
        {
            var info = _levelFiles.First(p => p.Name == Name);

            var level = new GameLevel(_componentsManager, info.Name, 0, 0, 0);

            level.Load(info.Path, _componentsManager);
        }

        public List<GameLevelInfo> GetLevelsInfo ()
		{
            if (_levelFiles == null)
                return null;

			return _levelFiles.ToList();
		}

        public List<string> GetLevelNames()
        {
            if (_levelFiles == null)
                return null;

            return _levelFiles.Select(p => p.Name).ToList();
        }

        public void RestartCurrent()
        {
            //CurrentLevelNumber = CurrentLevelNumber;
        }

        private bool LevelNameExists(string directory, string name, string type)
        {
            bool existInLevelFiles = _levelFiles != null && (_levelFiles.Any(p => p.Path == directory + name + type));

            bool existInLoadedLevels = LoadedLevels.Any(p => p.Name == name);
            


            return   !existInLevelFiles && !existInLoadedLevels ;
        }


    }
}

