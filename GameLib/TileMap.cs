
using System;
using System.Diagnostics;
using System.IO;

namespace GameLib
{
    public class TileMap
    {
        Int16[,] _map;
        
        private int _tileSize;
        private int _width;
        private int _height;
        
        public int TileSize
        {
            get 
            {
                return _tileSize;
            }
            set 
            {
                _tileSize = value;
                ResizeMap();
            }
        }
        
        public int Width
        {
            get { return _width; }
            set 
            {
                _width = value;
                ResizeMap();
            }
        }
        
        public int Height
        {
            get { return _height; }
            set
            {
                _height = value;
                ResizeMap();
            }
            
        }

        public TileMap(int width, int height)
        {
            _map = new Int16[width, height];
            _width = width;
            _height = height;
        }
        
        public void Load(string path)
        {
            Debug.Assert(path != null || path != "", "Path can not be null or empty");
            
            var stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.None);
            var reader = new BinaryReader(stream);
            
            _width = reader.ReadInt32();
            _height = reader.ReadInt32();
            
            ResizeMap();

            for (int i = 0; i < _width; i++)
                for (int j = 0; j < _height; j++)
                    _map[i, j] = reader.ReadInt16();
            
            stream.Close();
        }   
        public void Load(TileMap tileMap)
        {
            _width = tileMap.Width;
            _height = tileMap.Height;
            _tileSize = tileMap.TileSize;

            _map = new short[Width, Height];

            for (int i = 0; i < Width; i++)
                for (int j = 0; j < Height; j++) 
                    _map[i,j] = tileMap._map[i,j];//todo: prabably optimization needed
        }
        
        public void Save(string path)
        {
            Debug.Assert(path != null || path != "", "Path can not be null or empty");

            Directory.CreateDirectory(Path.GetDirectoryName(path));
            var stream = new System.IO.FileStream(path, FileMode.Create);
            var writer = new System.IO.BinaryWriter(stream);
            
            writer.Write(_width);
            writer.Write(_height);
            
            for (int i = 0; i < _width; i++)
                for (int j = 0; j < _height; j++)                
                    writer.Write(_map[i, j]);
            
            stream.Close();
        }
        
        private void ResizeMap()
        {
            Int16[,] oldmap = new Int16[Width, Height];
            Array.Copy(_map, oldmap, _map.GetLength(0) * _map.GetLength(1));
            //todo: add changes in position if tileSize became smaller
            _map = new Int16[_width, _height];
            
            for (int i = 0; i < oldmap.GetLength(0) && i < _width; i++)
                for (int j = 0; j < oldmap.GetLength(1) && j < _height; j++)
                    _map[i, j] = oldmap[i, j];
        }

        public void Draw()
        {
            //todo: tiles drawing
        }
    }
}

