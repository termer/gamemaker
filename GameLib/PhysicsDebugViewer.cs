﻿using Blueberry.Graphics;
using FarseerPhysics;
using FarseerPhysics.Collision.Shapes;
using FarseerPhysics.Common;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using OpenTK.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace GameLib
{
    public class PhysicsDebugViewer : DebugView
    {

        public PhysicsDebugViewer(World world)
            :base(world)
        {

        }


        public override void DrawPolygon(Microsoft.Xna.Framework.Vector2[] vertices, int count, float red, float blue, float green)
        {
            SpriteBatch.Instance.OutlinePolygon(vertices.Select<Vector2, OpenTK.Vector2>(p => new OpenTK.Vector2(p.X, p.Y)), new Color4(red, blue, green, 1.0f));
        }


        public override void DrawSolidPolygon(Microsoft.Xna.Framework.Vector2[] vertices, int count, float red, float blue, float green)
        {
            var v = vertices.Select<Vector2,OpenTK.Vector2>(p => new OpenTK.Vector2(p.X, p.Y)).ToList();
            SpriteBatch.Instance.FillPolygon(v, new Color4(red, blue, green, 1.0f));
        }

        public override void DrawCircle(Microsoft.Xna.Framework.Vector2 center, float radius, float red, float blue, float green)
        {
            SpriteBatch.Instance.OutlineCircle(center.X, center.Y, radius, new Color4(red, blue, green, 1.0f), 1.0f, 16);
        }

        public override void DrawSolidCircle(Microsoft.Xna.Framework.Vector2 center, float radius, Microsoft.Xna.Framework.Vector2 axis, float red, float blue, float green)
        {
            SpriteBatch.Instance.FillCircle(center.X, center.Y, radius, new Color4(red, blue, green, 1.0f), new Color4(blue, red, green, 1.0f), 16);
        }

        public override void DrawSegment(Microsoft.Xna.Framework.Vector2 start, Microsoft.Xna.Framework.Vector2 end, float red, float blue, float green)
        {
            SpriteBatch.Instance.DrawLine(start.X, start.Y, end.X, end.Y, 1, new Color4(red, blue, green, 1.0f));
        }

        public override void DrawTransform(ref FarseerPhysics.Common.Transform transform)
        {
            // throw new NotImplementedException();
        }

        /// <summary>
        /// Call this to draw shapes and other debug draw data.
        /// </summary>
        public void DrawDebugData()
        {
            {
                foreach (Body b in World.BodyList)
                {
                    Transform xf;
                    b.GetTransform(out xf);
                    foreach (Fixture f in b.FixtureList)
                    {
                        if (b.Enabled == false)
                            DrawShape(f, xf, Color4.White);
                        else
                            DrawShape(f, xf, Color4.Red);
                    }
                }
            }
        }
        public void DrawShape(Fixture fixture, Transform xf, Color4 color)
        {
            switch (fixture.ShapeType)
            {
                case ShapeType.Circle:
                    {
                        CircleShape circle = (CircleShape)fixture.Shape;
                        
                        Vector2 center = MathUtils.Multiply(ref xf, circle.Position);
                        float radius = circle.Radius;
                        Vector2 axis = xf.R.Col1;

                        DrawSolidCircle(center, radius, axis, color.R,color.B, color.G);
                    }
                    break;

                case ShapeType.Polygon:
                    {
                        PolygonShape poly = (PolygonShape)fixture.Shape;
                        int vertexCount = poly.Vertices.Count;
                        Debug.Assert(vertexCount <= Settings.MaxPolygonVertices);

                        var _tempVertices = new Vector2[vertexCount];


                        for (int i = 0; i < vertexCount; ++i)
                        {
                            _tempVertices[i] = MathUtils.Multiply(ref xf, poly.Vertices[i]);
                        }

                        DrawSolidPolygon(_tempVertices, vertexCount, color.R, color.B, color.G);
                    }
                    break;
            }
        }

    }
}
