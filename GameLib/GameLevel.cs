﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Blueberry.Graphics;
using GameLib.GameObjects;
using System.Drawing;
using System.Diagnostics;
using System.IO;
using GameLib.GameObjects.Components.Default;
using GameLib.GameObjects.Graphics;
using OpenTK;
using System.Xml;

namespace GameLib
{
    public class GameLevel
    {
        public BaseGameObjectsManager GameObjects{get; private set;}
        public TileMap TileMap{ get; private set; } 
		public string Name;


        public GameLevel(IComponentsManager componentsManager, string name, int width, int height, int tileSize)
        {
            Debug.Assert(width != 0 && height != 0, "Empty map");
			Name = name;

            TileMap = new TileMap(width, height)
            {
                TileSize = tileSize 
            };

            GameObjects = new PrototypeManager();

            
        }

        public void CopyLevelToGameWorld()
        {
            GameWorld.TileMap.Load(TileMap);
        }

        public void AddGameObject(IGameObject gameObject)
        {
            Debug.Assert(gameObject != null, "Can not be null");
			GameObjects.Add(gameObject);
        }

        public void RemoveGameObject(IGameObject gameObject)
        {
            Debug.Assert(gameObject != null, "Can not be null");
			GameObjects.Remove(gameObject);
        }

        public IGameObject GetGameObjectInPosition(PointF position)
        {
            return GetGameObjectInPosition(position.X, position.Y);
        }

        public IGameObject GetGameObjectInPosition(float x, float y)
        {
            //foreach (var item in _gameObjects)
            //{
            //    if (item.Transform.Collides(x, y))
            //        return item;
            //}
            return null;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="directory">DIrectroy in which level with current name is placed</param>
		public void Load (string directory, IComponentsManager componentsManager)
		{
			XmlDocument file = new XmlDocument();
            file.Load(directory +"//" + Name + ".lvl");
			XmlNode node = file.SelectSingleNode("Level");

			Name = node.SelectNodes("Name")[0].InnerText;



			TileMap.Load(directory + "\\" + Name + ".tm");
			GameObjects.Load(directory + "\\" + Name + "go.xml", componentsManager);
		}

		public void Save (string directory)
		{
			using (XmlWriter xmlWriter = XmlWriter.Create(directory + "//" + Name + ".lvl")) 
			{
				xmlWriter.WriteStartDocument();
				xmlWriter.WriteStartElement("Level");
				xmlWriter.WriteElementString("Name", Name);
				xmlWriter.WriteEndElement();
			}
			TileMap.Save(directory +"//" + Name + ".tm");
			GameObjects.Save(directory + "//" + Name + "go.xml");
		}


        public void Draw()
        {
            TileMap.Draw();
          //  foreach (var gameObject in _gameObjects)
            {
                //todo: add some drawing for invicible objects
              //  gameObject.Draw(0f);
            }
        }




    }
}
