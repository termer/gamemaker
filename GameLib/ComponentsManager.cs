﻿using System.IO;
using System.Reflection;
using GameLib.GameObjects;
using GameLib.GameObjects.Components;
using Microsoft.CSharp;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace GameLib
{

    public class ComponentsManager:BaseComponentsManager
    {
        public void LoadAssembly(string assembly)
        {
            Assembly componentsAssembly = Assembly.LoadFile(assembly);
            var assemblyTypes = componentsAssembly.GetExportedTypes();
            foreach (var type in assemblyTypes)
            {
                if (type.BaseType == typeof (IComponent)) 
                    Components.Add(type.Name, type);
            }
        }
    }
}
