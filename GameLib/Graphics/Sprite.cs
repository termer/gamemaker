﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Blueberry.Animations;
using Blueberry.Graphics;
using OpenTK;
using System.Drawing;
using OpenTK.Graphics;

namespace GameLib.GameObjects.Graphics
{
    public class Sprite  
    {
        public Color4 Color;

        private float _imageSpeed;

        public Rectangle Bounds(Vector2 position)
        {
            return new Rectangle((int)position.X, (int)position.Y, _image.Size.Width, _image.Size.Height);
        }

        public float ImageSpeed
        {
            get { return _imageSpeed; }
            set 
            {
                _imageSpeed = value;
             //   if (_frameAnimation != null)
                   // _frameAnimation.Period = value;//todo: change it

            }
        }

        FrameAnimation _frameAnimation;
        Texture _image;
        private int _frameWidth;

        public Sprite(Texture image)
            :this(image, image.Size.Width){}

        public Sprite(Texture image, int frameWidth)
        {
            _frameWidth = frameWidth;
            SetImage(image, _frameWidth);
            Color = Color4.White;
        }

        public void SetImage(Texture image, int frameWidth)
        {
            _image = image;

            _frameAnimation = new FrameAnimation(false, frameWidth, image.Size.Height, 0,
                                                 image.Size.Width / frameWidth, ImageSpeed);
        }

        public void Animate(float dt)
        {
            _frameAnimation.Animate(dt);
        }
        public void Draw(Vector2 position, float rotation)
        {
            SpriteBatch.Instance.DrawTexture(_image, position.X, position.Y, (Rectangle)_frameAnimation, Color, rotation, 0.5f, 0.5f, 1, 1);
        }

    }
}
