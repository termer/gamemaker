﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using GameLib.GameObjects.Messages;

namespace GameLib.GameObjects.Components
{
    public interface IComponent: ICloneable
    {
        string Name { get; set; }

        FieldInfo[] Fields { get; }

        object this[string name] { get; set; }
        object this[FieldInfo field] { get; set; }

        IGameObject Owner { get; set; }

        void SetComponent(IComponent component);

        void ProccesMessage(IMessage message);
        bool GetDependicies();

        void Save(XmlWriter xmlWriter);
        void Load(XmlNode node);
    }
}
