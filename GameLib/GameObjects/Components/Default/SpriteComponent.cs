﻿using GameLib.GameObjects.Graphics;
using GameLib.GameObjects.Messages;

namespace GameLib.GameObjects.Components.Default
{
    public class SpriteComponent:Component, IDrawable,IUpdatable
    {
        Transform _transform;
        public Sprite Sprite;

        public void SetSprite(Sprite sprite)
        {
            Sprite = sprite;
        }


        public override bool GetDependicies()
        {
            _transform = Owner.GetComponent<Transform>();
            return _transform != null;
        }

        public override void ProccesMessage(IMessage message)
        {
        }

        public void Update(float dt)
        {
            Sprite.Animate(dt);
        }

        public void Draw(float dt)
        {
            Sprite.Draw(_transform.Position, _transform.Rotation);
        }


    }
}
