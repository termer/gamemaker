﻿using System;
using System.Collections.Generic;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using GameLib.GameObjects.Messages;
using OpenTK;

namespace GameLib.GameObjects.Components.Default
{
    public class PhysicsComponent:Component, IUpdatable, IDisposable
    {
        public Vector2 Position
        {
            get 
            {
                return new Vector2(Body.WorldCenter.X, Body.WorldCenter.Y);
            }
        }

        public Vector2 Velocity
        {
            get 
            {
                return new Vector2(Body.LinearVelocity.X, Body.LinearVelocity.Y);
            }
        }

        internal List<IGameObject> Collisions{get; private set;}

        protected Body Body{get; set;}

        private bool _wasUpdate = false;
        private bool _wasCollisionMessage = false;

        public PhysicsComponent()
            :base()
        {
            Body = new Body(GameWorld.PhysicsWorld);
            Body.Mass = 5;
            Body.BodyType = BodyType.Dynamic;
            Body.UserData = this;

            Collisions = new List<IGameObject>();
        }

        public override bool GetDependicies()
        {
            
            return base.GetDependicies();
        }

        public void Translate(float x, float y)
        {
            var ver = new Microsoft.Xna.Framework.Vector2(x, y);
            Body.SetTransformIgnoreContacts(ref ver, Body.Rotation);
        }

        public void Push(Vector2 direction, float speed)
        {
            Push(direction * speed);
        }
        public void Push(Vector2 velocity)
        {
            Body.ApplyForce(new Microsoft.Xna.Framework.Vector2(velocity.X, velocity.Y));
        }

        public void SetVelocity(Vector2 velocity)
        {
            Body.LinearVelocity = new Microsoft.Xna.Framework.Vector2(velocity.X, velocity.Y);
        }

        public void AddRectangleShape(float width, float height, float offsetX, float offsetY, float density)
        {
            Fixture rect = FixtureFactory.AttachRectangle(width, height, density,
                                                          new Microsoft.Xna.Framework.Vector2(offsetX, offsetX),
                                                          Body);
        }

        public void AddCircleShape(float radius, float offsetX, float offsetY, float density)
        {
            Fixture rect = FixtureFactory.AttachCircle(radius, density, Body,
                                                       new Microsoft.Xna.Framework.Vector2(offsetX, offsetX));
        }

        public override void ProccesMessage(Messages.IMessage message)
        {
            if (message is CollisionMessage)
            {
                HandleCollisionMessage((CollisionMessage)message);
            }
        }

        private void HandleCollisionMessage(CollisionMessage message)
        {
            if (_wasUpdate)
            {
                Collisions.Clear();
                _wasUpdate = false;
            }
            Collisions.Add(message.CollidedObject);
            _wasCollisionMessage = true;
        } 

        public void Update(float dt)
        {
            if (!_wasCollisionMessage)
                Collisions.Clear();

            Owner.Transform.Position = Position;
            Owner.Transform.Rotation = Body.Rotation;
            _wasUpdate = true;
            _wasCollisionMessage = false;
        }

        public void Dispose()
        {

        }
    }
}
