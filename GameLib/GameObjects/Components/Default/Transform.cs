﻿using System.Drawing;
using OpenTK;

namespace GameLib.GameObjects.Components.Default
{
    public class Transform:Component
    {
        public float X, Y;

        public float Width, Height;

        public SizeF Size
        {
            get
            {
                return new SizeF(Width, Height);
            }
            set 
            {
                Width = value.Width;
                Height = value.Height;
            }
        }

        public float Rotation;

        public Vector2 Position
        {
            get 
            { 
                return new Vector2(X, Y); 
            }
            set
            {
                X = value.X;
                Y = value.Y;
            }
        }


        public Vector2 Center
        {
            get { return new Vector2(Position.X + Size.Width, Position.Y + Size.Height); }
        }



        //public Point Cell
        //{
        //    get
        //    {
        //        return new Point((int)(Position.X / 64), (int)(Position.Y / 64));
        //    }
        //    set
        //    {

        //        _x = value.X * 64 + Size.Width / 2; _y = value.Y * 64 + Size.Height / 2;
        //    }
        //}

        //private SizeF _size;
        //public SizeF Size
        //{
        //    get { return new SizeF(_size.Width, _size.Height); }
        //    set { _size.Width = value.Width; _size.Height = value.Height; }
        //}

        //public virtual RectangleF Bounds
        //{
        //    get
        //    {
        //        return new RectangleF((Position.X - (float)Size.Width / 2), (Position.Y - (float)Size.Height / 2), Size.Width, Size.Height);
        //    }
        //}



        //public float Left { get { return Bounds.Left; } }
        //public float Right { get { return Bounds.Right; } }
        //public float Top { get { return Bounds.Top; } }
        //public float Bottom { get { return Bounds.Bottom; } }



        public Transform()
            : base()
        {
            Name = "Transform";
        }

        public override void ProccesMessage(Messages.IMessage message)
        {

        }

        static public float Distance(Transform b1, Transform b2)
        {
            return (b1.Position - b2.Position).LengthFast;
        }
    }
}
