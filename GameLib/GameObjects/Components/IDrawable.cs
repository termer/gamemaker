﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameLib.GameObjects.Components
{
    public interface IDrawable
    {
        void Draw(float dt);
    }
}
