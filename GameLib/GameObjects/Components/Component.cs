﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using GameLib.GameObjects.Messages;

namespace GameLib.GameObjects.Components
{
    public abstract class Component : IComponent
    {
        public string Name { get; set; }

        public IGameObject Owner { get; set; }

        public FieldInfo[] Fields { get; protected set; }

        public object this[string name]
        {
            get
            {
                return Fields.First(p => p.Name == name).GetValue(this);
            }
            set
            {
                Fields.First(p => p.Name == name).SetValue(this, value);
            }
        }
        public object this[FieldInfo field]
        {
            get
            {
                return this[field.Name];
            }
            set
            {
                this[field.Name] = value;
            }
        }


        public Component()
        {

            Fields = GetType().GetFields();
            Name = GetType().Name;
        }

        public void SetComponent(IComponent component)
        {
            foreach (var field in component.Fields)
            {
                try
                {
                    this[field] = component[field];
                }
                catch
                {
                    // if we don't have field with that name, then just ignore old field
                }
            }
        }


        public abstract void ProccesMessage(IMessage message);

        public virtual bool GetDependicies()
        {
            return true;
        }

        public void Save(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("Component");
            xmlWriter.WriteAttributeString("Name", Name);
            foreach (var item in Fields)
            {
                xmlWriter.WriteAttributeString(item.Name, item.GetValue(this).ToString());
            }
            xmlWriter.WriteEndElement();
        }

        public void Load(XmlNode node)
        {
            foreach (XmlAttribute element in node.Attributes)
            {
               // Type type = Type.GetType(attribute.Value);
               // _variables[attribute.Name] = Activator.CreateInstance(type);
                try
                {
                    var typeConverter = TypeDescriptor.GetConverter(GetField(element.Name).FieldType);

                    this[element.Name] = typeConverter.ConvertFrom(element.Value);
                }
                catch
                {
                    //if attributes name was changed - do nothing
                }
            }
        }

        public FieldInfo GetField(string name)
        {
            return Fields.FirstOrDefault(p => p.Name == name);
        }

        public object Clone()
        {
            var type = GetType();
            var clone = type.GetConstructor(Type.EmptyTypes).Invoke(null); 
            var field = type.GetFields();
            foreach (var f in field)
            {
                f.SetValue(clone, f.GetValue(this));
            }
            return clone;
        }
    }
}
