﻿namespace GameLib.GameObjects.Messages
{
    public class CollisionMessage:IMessage
    {
        public readonly IGameObject CollidedObject;

        public CollisionMessage(IGameObject gameObject)
        {
            CollidedObject = gameObject;
        }
    }
}
