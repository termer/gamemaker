﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Xml;
using GameLib.GameObjects.Components.Default;
using GameLib.GameObjects.Messages;
using GameLib.GameObjects.Components;

namespace GameLib.GameObjects
{
    public interface IGameObject: IDisposable, ICloneable
    {
        bool IsDrawable { get; }
        bool IsUpdateable { get; }

        event EventHandler OnRemove;
        
        Transform Transform { get; }

        /// <summary>
        /// non active objects will not be updated
        /// </summary>
        bool Active { get; set; }

        bool Visible { get; set; }

        /// <summary>
        /// if true object will be updated even beyond bounds of camera
        /// </summary>
        bool AutoChangeActivity { get; set; }

        string Name { get; set;}



        T GetComponent<T>() where T : IComponent;
        List<T> GetComponents<T>() where T : IComponent;


        void AddComponents(params IComponent[] component);
        void RemoveComponent<T>() where T : IComponent;
        void ClearComponents();

        void RecreateComponents(IComponentsManager componentsManager);

        void Update(float dt);
        void Draw(float dt);

        void SendMessage(IMessage message);

        void Save(XmlWriter xmlWriter);
        void Load(XmlNode node, IComponentsManager componentsManager);
    }
}
