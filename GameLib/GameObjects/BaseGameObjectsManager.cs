using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace GameLib.GameObjects
{
    public class BaseGameObjectsManager
    {
        protected readonly List<IGameObject> GameObjects;

        public BaseGameObjectsManager()
        {
            GameObjects = new List<IGameObject>();
        }

        public virtual void Add(IGameObject gameObject)
        {
            Debug.Assert(gameObject != null, "gameObject equals to null");
            GameObjects.Add(gameObject);
        }
        public virtual void Remove(IGameObject gameObject)
        {
            Debug.Assert(gameObject != null, "gameObject equals to null");
            GameObjects.Remove(gameObject);
        }

        public void Remove(string name)
        {
            GameObjects.RemoveAll(p => p.Name == name);
        }


        public virtual void Clear()
        {
            GameObjects.Clear();
        }
        
        public IGameObject GetByName(string name)
        {
            return GameObjects.FirstOrDefault(g => g.Name == name);
        }
        public IEnumerable<IGameObject> GetListName(string name)
        {
            return GameObjects.Where(g => g.Name == name);
        }


        public void Rename(string oldName, string newName)
        {
            GameObjects.First(p => p.Name == oldName).Name = newName;
        }

        public void Save(string directory)
        {
            using (XmlWriter xmlWriter = XmlWriter.Create(directory))
            {
                xmlWriter.WriteStartDocument();
                xmlWriter.WriteStartElement("GameObjects");

                foreach (var item in GameObjects)
                    item.Save(xmlWriter);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndDocument();
            }
        }

        public void Load(string directory, IComponentsManager componentsManager)
        {
            var file = new XmlDocument();
            file.Load(directory);
            XmlNode main = file.SelectSingleNode("GameObjects");
            XmlNodeList gameObjects = main.SelectNodes("GameObject");

            foreach (XmlNode item in gameObjects)
            {
                var editableGameObject = new GameObject("");
                editableGameObject.Load(item, componentsManager);
                Add(editableGameObject);
            }
        }

        
        public IGameObject[] CloneObjects()
        {
            return GameObjects.Select(p => (IGameObject)p.Clone()).ToArray();
        }

        public void RecreateComponents(IComponentsManager componentsManager, Type[] oldC, Type[] newC)
        {
            foreach (var gameObject in GameObjects)
                gameObject.RecreateComponents(componentsManager);
        }
    }
}

