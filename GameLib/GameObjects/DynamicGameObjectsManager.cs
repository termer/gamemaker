﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Blueberry.Graphics;
using GameLib.GameObjects.Messages;
using System.Drawing;
using GameLib.GameObjects.Components;
using System.Diagnostics;
using OpenTK.Graphics;

namespace GameLib.GameObjects
{
    public class DynamicGameObjectsManager:BaseGameObjectsManager
    {
        private Queue<IGameObject> _addNewObjectsQueue;
        private Queue<IGameObject> _deleteObjectsQueue;

        internal DynamicGameObjectsManager()
        {
            _addNewObjectsQueue = new Queue<IGameObject>(0);
            _deleteObjectsQueue = new Queue<IGameObject>(0);
        }

        internal void AddQueue(IGameObject gameObject)
        {
            Debug.Assert(gameObject != null, "gameObject equals to null");
            _addNewObjectsQueue.Enqueue(gameObject);
        }

        internal void RemoveQueue(IGameObject gameObject)
        {
            Debug.Assert(gameObject != null, "gameObject equals to null");
            _deleteObjectsQueue.Enqueue(gameObject);
        }

        internal void ClearQueue(params IGameObject[] ignoreList)
        {
            UpdateObjectsQueues();

            foreach (var gameObject in GameObjects.Where(gameObject => ignoreList.All(p => p != gameObject)))
                RemoveQueue(gameObject);

            UpdateObjectsQueues();
        }


        internal void SendGlobalMessage(IMessage message)
        {
            foreach (var gameObject in GameObjects)
                gameObject.SendMessage(message);
        }

        internal void Update(float dt)
        {
            for (int i = 0; i < GameObjects.Count; i++)
            {
                var gameObject = GameObjects[i];
                if(gameObject.AutoChangeActivity)
                    gameObject.Active = IsActive(gameObject);
                if (gameObject.Active)
                    gameObject.Update(dt);
            }
            UpdateObjectsQueues();
        }

        private bool IsActive(IGameObject gameObject)
        {
            return true;

          //  var bc = gameObject.GetComponent<Transform>();
          //  return bc == null;//|| _cameraRect.Contains(bc.Position);
        }

        internal void Draw(float dt)
        {
            for (int i = 0; i < GameObjects.Count; i++)
            {
                var gameObject = GameObjects[i];
                if (gameObject.Active)
                    gameObject.Draw(dt);
            }
        }

        private void UpdateObjectsQueues()
        {
            while (_addNewObjectsQueue.Count > 0)
            {
                IGameObject gameObject = _addNewObjectsQueue.Dequeue();
                GameObjects.Add(gameObject);
            }
            while (_deleteObjectsQueue.Count > 0)
            {
                IGameObject obj = _deleteObjectsQueue.Dequeue();
                obj.Dispose();
                GameObjects.Remove(obj);
            }
        }


        public void DrawInvicibleObjects()
        {
            for (int i = 0; i < GameObjects.Count; i++)
            {
                var gameObject = GameObjects[i];
                if(!gameObject.IsDrawable)
                {
                    SpriteBatch.Instance.FillRectangle(gameObject.Transform.X,
                                                       gameObject.Transform.Y,
                                                       gameObject.Transform.Width,
                                                       gameObject.Transform.Height,
                                                       Color4.Blue);
                }
            }
        }
    }
}
