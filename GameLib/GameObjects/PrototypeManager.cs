﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GameLib.GameObjects
{
    public class PrototypeManager:BaseGameObjectsManager
    {

        public PrototypeManager()
            :base()
        {
        }

        public List<IGameObject> GetGameObjects()
        {
            return GameObjects.ToList();
        }

        public IGameObject CreateGameObject(string name)
        {
            return (IGameObject)GameObjects.First(p => p.Name == name).Clone();
        }


    }
}
