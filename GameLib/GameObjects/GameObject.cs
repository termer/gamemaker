﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Xml;
using GameLib.GameObjects.Components;
using GameLib.GameObjects.Components.Default;
using GameLib.GameObjects.Messages;

namespace GameLib.GameObjects
{
    public class GameObject:IGameObject
    {
        public event EventHandler OnRemove;



        public bool Active { get; set; }
        public bool Visible { get; set; }
        public bool AutoChangeActivity { get; set; }

        public bool IsDrawable
        {
            get { return DrawableComponents.Count > 0; }
        }

        public bool IsUpdateable
        {
            get { return UpdatableComponents.Count > 0; }
        }


        public string Name
        {
            get;
            set;
        }

        public Transform Transform
        {
            get;
            private set;
        }

        protected List<IComponent> Components;
        protected List<IUpdatable> UpdatableComponents;
        protected List<IDrawable> DrawableComponents;


        public GameObject(string name)
        {
			Active = true;

            Components = new List<IComponent>();
            UpdatableComponents = new List<IUpdatable>();
            DrawableComponents = new List<IDrawable>();

            AutoChangeActivity = true;

            AddComponents(Transform = new Transform());

            Name = name;
        }


        public T GetComponent<T>() where T : IComponent
        {
            for (int i = 0; i < Components.Count; i++)
            {
                IComponent p = Components[i];
                if (p is T)
                    return (T) p;
            }
            return default(T);
        }

        public List<T> GetComponents<T>() where T : IComponent
        {
            List<T> list = new List<T>();
            for (int i = 0; i < Components.Count; i++)
            {
                IComponent p = Components[i];
                if (p is T) list.Add((T) p);
            }
            return list;
        }

        public virtual void AddComponents(params IComponent[] components)
        {

            foreach (var component in components)
            {
                Components.Add(component);
                if (component is IUpdatable)
                    UpdatableComponents.Add((IUpdatable)component);
                if (component is IDrawable)
                    DrawableComponents.Add((IDrawable)component);
                component.Owner = this;                
            }
            foreach (IComponent component in Components)
                if (!component.GetDependicies())
                    Debug.Print("Depedicies were not found " + component);
        }

        public void RecreateComponents(IComponentsManager componentsManager)
        {
            for (int i = 0; i < Components.Count; i++)
            {
                var c = Components[i];
                c.SetComponent(componentsManager.CreateComponent(c.Name));
            }
        }

        public void Update(float dt)
        {
            for (int i = 0; i < UpdatableComponents.Count; i++)
            {
                var component = UpdatableComponents[i];
                component.Update(dt);
            }
        }

        public void Draw(float dt)
        {
            for (int i = 0; i < DrawableComponents.Count; i++)
            {
                var component = DrawableComponents[i];
                component.Draw(dt);
            }
        }

        public void SendMessage(IMessage message)
        {
            for (int index = 0; index < Components.Count; index++)
            {
                var component = Components[index];
                component.ProccesMessage(message);
            }
        }

        public void RemoveComponent<T>() where T : IComponent
        {
            IComponent component = GetComponent<T>();
            if (component != null)
            {
                Components.Remove(component);
                if(component is IUpdatable)
                {
                    UpdatableComponents.Remove((IUpdatable)component);
                }
                if(component is IDrawable)
                {
                    DrawableComponents.Remove((IDrawable)component);
                }
            }
        }

        public void ClearComponents()
        {
            Components.Clear();
            UpdatableComponents.Clear();
            DrawableComponents.Clear();
        }

        public void Save(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("GameObject");
            xmlWriter.WriteAttributeString("Name", Name);
            xmlWriter.WriteStartElement("Components");
            foreach (var item in Components)
            {
                item.Save(xmlWriter);
            }
            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndElement();
        }

        public void Load(XmlNode node, IComponentsManager componentsManager)
        {
            Name = node.Attributes["Name"].Value;
            var componentsNode = node.SelectSingleNode("Components");
            var componentsNodes = componentsNode.SelectNodes("Component");

            foreach (XmlNode item in componentsNodes)
            {
                //var componentData = new Component();
                var component = componentsManager.CreateComponent(item.Attributes["Name"].Value);
                component.Load(item);
                AddComponents(component);
                if (component.GetType() == typeof(Transform))
                    Transform = (Transform)component;


             //   componentData.SetComponent(component);
             //   componentData.Load(item);
            }
        }

        public void Dispose()
        {
            foreach (var component in Components)
            {
                if(component is IDisposable)
                    (component as IDisposable).Dispose();
            }
            if (OnRemove != null)
                OnRemove(this, EventArgs.Empty);
            OnRemove = null;
        }
        public object Clone()
        {
            var clone = new GameObject(Name);
            clone.ClearComponents();

            var components = new IComponent[Components.Count];

            clone.Transform = (Transform)Transform.Clone();
            components[0] = clone.Transform;
            for (int i = 1; i < components.Length; i++)
                components[i] = (IComponent)Components[i].Clone();

            clone.AddComponents(components);
            return clone;
        }

    }
}
