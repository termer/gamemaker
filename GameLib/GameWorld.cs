﻿using FarseerPhysics.Dynamics;
using GameLib.GameObjects;
using GameLib.GameObjects.Components;
using GameLib.GameObjects.Components.Default;
using GameLib.GameObjects.Messages;
using OpenTK;
using XnaVector = Microsoft.Xna.Framework.Vector2;

namespace GameLib
{
    static public class GameWorld
    {
        static public TileMap TileMap{get; private set;}
        static public DynamicGameObjectsManager GameObjects { get; private set; }
        static public World PhysicsWorld { get; private set; }

        static GameWorld()
        {
            PhysicsWorld = new World(new XnaVector(0, 0));
    //        PhysicsWorld.ContactManager.ContactList[0]..OnBroadphaseCollision += OnCollision;
            GameObjects = new DynamicGameObjectsManager();
            TileMap = new TileMap(0,0);
             
        }

        static public void Update(float dt)
        {
            GameObjects.Update(dt);

            PhysicsWorld.Step(dt);

            foreach (var contact in PhysicsWorld.ContactManager.ContactList)
            {
                var pc1 = (PhysicsComponent)contact.FixtureA.Body.UserData;
                var pc2 = (PhysicsComponent)contact.FixtureB.Body.UserData;

                var ow1 = pc1.Owner;
                var ow2 = pc2.Owner;

                ow1.SendMessage(new CollisionMessage(ow2));
                ow2.SendMessage(new CollisionMessage(ow1));
            }
        }

        static public void Draw(float dt)
        {
            GameObjects.Draw(dt);
        }

        static public void DrawInvicibleObjects()
        {
            GameObjects.DrawInvicibleObjects();
        }


        static private void OnCollision(ref FixtureProxy proxyA, ref FixtureProxy proxyB)
        {

        }
    }
}
