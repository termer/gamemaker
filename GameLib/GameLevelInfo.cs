﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameLib
{
    public struct GameLevelInfo
    {
        public string Directory;
        public string Name;

        public string Path
        {
            get { return Directory + Name; }
            set 
            {
                Directory = System.IO.Path.GetDirectoryName(value);
                Name = System.IO.Path.GetFileName(value);
                Name = Name.Replace(".lvl", "");
            }
        }

        //public void LoadGameLevel(GameLevel gameLevel)
        //{
        //    gameLevel.Name = Name;
        //    gameLevel.Load(Directory);
        //}
    }
}
