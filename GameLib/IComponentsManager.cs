﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameLib.GameObjects;
using GameLib.GameObjects.Components;

namespace GameLib
{
    public delegate void AssemblyChangedEventHandler(object sender, Type[] newTypes, Type[] oldTypes);
    public interface IComponentsManager
    {
        event AssemblyChangedEventHandler AssemblyChanged;
        string Directory { get; set; }

        IComponent CreateComponent(string name);
    }
}
