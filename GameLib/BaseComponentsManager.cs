﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using GameLib.GameObjects.Components;
using GameLib.GameObjects.Components.Default;

namespace GameLib
{
    public class BaseComponentsManager:IComponentsManager
    {
        public event AssemblyChangedEventHandler AssemblyChanged;
        public virtual string Directory {  get; set; }
        protected Dictionary<string, Type> Components;


        
        public BaseComponentsManager()
        {
            Components = new Dictionary<string, Type>();
            LoadDefaultComponents();
        }

        public void LoadDefaultComponents()
        {
            AddType(typeof (Transform));
            //todo: add more

        }
        public void AddType(Type componentType)
        {
            if(componentType.GetInterfaces().Any(p => p == typeof(IComponent)))
                Components.Add(componentType.Name, componentType);
        }

        public IComponent CreateComponent(string name)
        {
            var constructor = Components[name].GetConstructor(new Type[]{});

            Debug.Assert(constructor != null, "constructor != null");

            var component = (IComponent)constructor.Invoke(null);
            if(string.IsNullOrEmpty(component.Name))
            {
                component.Name = name;
            }
            return component;
        }

        public string[] GetComponentsNames()
        {
            return Components.Keys.ToArray();
        }

        protected void OnAssemblyChanged(object sender, Type[] newTypes, Type[] oldTypes)
        {
            if (AssemblyChanged != null)
                AssemblyChanged(sender, newTypes, oldTypes);
        }

    }
}
