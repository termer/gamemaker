﻿using System.Linq;

namespace StandartComponents
{
    public enum PlatformState
    {
        InAir,
        OnGround
    }
    public enum MoveDirection
    {
        Left,
        Right,
        Up,
        Down
    }


    public class PlatformControl : Component, IUpdatable
    {
        public bool OnGround
        {
            get;
            protected set;
        }

        public bool Jump { get; protected set; }
        public bool InAir { get { return !OnGround; } }


        public float GroundAcceleration;
        public float AirAcceleration;

        public float StartJumpSpeed;
        public float JumpAccel;

        public float JumpLength;

        private float _jumpElapsed;


        private PhysicsComponent _physicsComponent;       

        public PlatformControl()
            : base()
        {
        }

        public override bool GetDependicies()
        {
            _physicsComponent = Owner.GetComponent<PhysicsComponent>();
            return _physicsComponent != null;
        }

        public override void ProccesMessage(Messages.IMessage message)
        {
            //if (message is CollisionMessage)
            //{
            //    var cm = (CollisionMessage)message;

            //}
        }

        public void Move(MoveDirection direction)
        {
            var dir = new Vector2(direction == MoveDirection.Left ? -1 : 1, 0);

            if(OnGround)
                _physicsComponent.Push(dir, GroundAcceleration);
            else
                _physicsComponent.Push(dir, AirAcceleration);
        }

        public void StartJump()
        {
            if (!OnGround)
                return;
            Jump = true;
            _jumpElapsed = 0;
        }
        public void StopJump()
        {
            Jump = false;
            _physicsComponent.SetVelocity(new Vector2(_physicsComponent.Velocity.X, 0));
        }
        public void Update(float dt)
        {
            CheckGround();
            UpdateJump(dt);

        }

        private void CheckGround()
        {
            OnGround = false;
            var walls = _physicsComponent.Collisions.Where(g => g.GetComponent<WallComponent>() != null);

            foreach(var wall in walls)
            {
                if (wall.Transform.Position.Y > Owner.Transform.Position.Y)
                    OnGround = true;
            }
        }

        private void UpdateJump(float dt)
        {
            if (Jump)
            { 
                _jumpElapsed += dt;
                if (_jumpElapsed >= dt)
                {
                    dt = 1 - _jumpElapsed;
                    _jumpElapsed = 0;
                    Jump = false;
                }
                _physicsComponent.Push(new Vector2(0, -JumpAccel * dt));
            }
        }
    }
}
