﻿namespace StandartComponents
{
    public class WallComponent:PhysicsComponent
    {
        public WallComponent(World physicsWorld)
            : base(physicsWorld)
        {
            Body.BodyType = BodyType.Static;
        }

        public override void ProccesMessage(Messages.IMessage message)
        {
        }
    }
}
